package com.gitlab.semantor.restful.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.semantor.restful.entity.Client;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.sql.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-local.properties")
@Sql({"classpath:schema.sql"})
class ClientControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private MockMvc mockMvc;

    private TestRestTemplate restTemplate = new TestRestTemplate();

    @Autowired
    private ObjectMapper mapper;

    @Test
    void getClientByID() throws Exception {
        mockMvc.perform(get("/client/{id}", 1).accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Vasiliy"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.surname").value("Zaycev"));
    }

    @Test
    void updateClientById() throws Exception {
        Client client1 = new Client("Ivan", "Ivanov", Date.valueOf("1920-12-12"));
        String client = mapper.writeValueAsString(client1);
        mockMvc.perform(post("/client/{id}", 3)
                .content(client)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Ivan"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.surname").value("Ivanov"));
    }


    @Test
    void getClient() {
        Client client = new Client("Ivan", "Kozhedub", Date.valueOf("1920-01-08"));

        ResponseEntity<Client> responseEntity = restTemplate.getForEntity("http://localhost:" + port + "/client/3", Client.class);

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(responseEntity.getBody().getName(), client.getName());
        assertEquals(responseEntity.getBody().getSurname(), client.getSurname());
    }

    @Test
    void createClient() {
        Client client = new Client("Ivan", "Ivanov", Date.valueOf("1920-12-12"));

        ResponseEntity<Client> responseEntity = restTemplate.postForEntity("http://localhost:" + port + "/client", client, Client.class);

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(responseEntity.getBody().getName(), client.getName());
        assertEquals(responseEntity.getBody().getSurname(), client.getSurname());
    }

}