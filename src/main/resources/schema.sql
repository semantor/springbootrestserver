DROP TABLE IF EXISTS payments;
DROP TABLE IF EXISTS bank_accounts;
DROP TABLE IF EXISTS clients;

CREATE TABLE clients
(
    client_id serial PRIMARY KEY,
    name      VARCHAR(250) NOT NULL,
    surname   VARCHAR(250) NOT NULL,
    birthday  date DEFAULT NULL
);
CREATE TABLE bank_accounts
(
    bank_account_id     serial primary key,
    bank_account_number bigint default 0,
    balance             int    default 0,
    client_id           int,
    foreign key (client_id) references clients (client_id)
);

CREATE TABLE payments
(
    payment_id      serial primary key,
    bank_account_id int  not null,
    price           int  not null,
    date            date default current_date,
    status          varchar default 'NOT_STARTED',
    foreign key (bank_account_id) references bank_accounts (bank_account_id)
);


insert into clients(name, surname, birthday)
values ('Vasiliy', 'Zaycev', '1915-03-23'),
       ('Dmitrii', 'Lavrinenko', '1914-10-01'),
       ('Ivan', 'Kozhedub', '1920-01-08');

insert into bank_accounts(bank_account_number, balance, client_id)
values (1234457812344575, 250000, 1),
       (1234412312344575, 100000, 1),
       (4567349673496754, 100000, 1),
       (2384728934793273, 300000, 2);

insert into payments(bank_account_id, price)
VALUES (1,100),(1,150),(1,300),(2,450),(2,1234),(4,233);
