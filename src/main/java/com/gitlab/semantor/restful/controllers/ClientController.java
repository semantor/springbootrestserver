package com.gitlab.semantor.restful.controllers;


import com.gitlab.semantor.restful.entity.BankAccount;
import com.gitlab.semantor.restful.entity.Client;
import com.gitlab.semantor.restful.entity.Payment;
import com.gitlab.semantor.restful.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.util.List;

@RestController
public class ClientController {

    @Autowired
    private ClientService clientService;

    @GetMapping("/client")
    public List<Client> getAllClient() {
        return clientService.getAllClient();
    }

    @GetMapping(value = "/client/{id}")
    public Client getClientByID(@PathVariable("id") int id) {
        return clientService.getClientByID(id);
    }

    @GetMapping(value = "/client/{id}/bankaccount")
    public List<BankAccount> getBankAccountByClient(@PathVariable("id") int id) {
        return clientService.getBankAccountByClient(id);
    }

    @GetMapping(value = "/client/{id}/payments")
    public List<Payment> getClientPayments(@PathVariable("id") int id) {
        return clientService.getClientPayments(id);
    }


    @PostMapping(value = "/client")
    public Client createNewClient(@RequestBody Client client) {
        return clientService.createNewClient(client);
    }

    @PostMapping(value = "/client/{id}")
    public Client updateClient(@PathVariable("id") int id, @RequestBody Client client) {
        return clientService.updateClientByID(id, client);
    }

    @DeleteMapping(value = "/client/{id}")
    public ResponseEntity deleteClientByID(@PathVariable("id") int id, HttpServletResponse response) {
        clientService.deleteClientByID(id);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "client/search")
    public List<Client> getByNameOrSurname(String name) {
        return clientService.getClientByName(name);
    }

    @GetMapping(value = "client/search/age")
    public List<Client> getYoungest(Date date) {
        return clientService.getYoungClient(date);
    }

}
