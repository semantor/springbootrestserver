package com.gitlab.semantor.restful.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@ToString
public enum Status implements Serializable {
    NOT_STARTED("NOT_STARTED"), PROCESSING("PROCESSING"), COMPLETED("COMPLETED");
    @Getter
    @Setter
    @JsonProperty
    private String status;

    private Status(String status) {
        this.status = status;
    }


}
