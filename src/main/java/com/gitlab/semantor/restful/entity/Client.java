package com.gitlab.semantor.restful.entity;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name = "clients")
@Data
@NoArgsConstructor
public class Client implements Serializable {

    public static final Client defaultClient = new Client("name", "surname", Date.valueOf("1970-01-01"));

    @Id
    @Column(name = "client_id")
    @EqualsAndHashCode.Exclude
    @Setter(AccessLevel.NONE)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty
    private int id;
    @Column(name = "name")
    @JsonProperty
    private String name;
    @Column(name = "surname")
    @JsonProperty
    private String surname;
    @Column(name = "birthday")
    @JsonProperty
    private Date birthday;


    public Client(String name, String surname, Date birthday) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
    }

    public Client(int id, String name, String surname, Date birthday) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
    }

}
