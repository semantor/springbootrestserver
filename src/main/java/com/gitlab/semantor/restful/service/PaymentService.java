package com.gitlab.semantor.restful.service;

import com.gitlab.semantor.restful.dao.BankAccountDAO;
import com.gitlab.semantor.restful.dao.PaymentDAO;
import com.gitlab.semantor.restful.entity.BankAccount;
import com.gitlab.semantor.restful.entity.Payment;
import com.gitlab.semantor.restful.exceptions.DatabaseException;
import com.gitlab.semantor.restful.exceptions.EntityNotFoundException;
import com.gitlab.semantor.restful.exceptions.NotEnoughDataException;
import com.gitlab.semantor.restful.exceptions.NotEnoughMoneyException;
import lombok.AccessLevel;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class PaymentService {

    @Getter(AccessLevel.PROTECTED)
    private final PaymentDAO paymentDAO;
    @Getter(AccessLevel.PROTECTED)
    private final BankAccountDAO bankAccountDAO;
    @Getter(AccessLevel.PROTECTED)
    private final BankAccountService service;

    public PaymentService(PaymentDAO paymentDAO, BankAccountDAO bankAccountDAO, BankAccountService service) {
        this.paymentDAO = paymentDAO;
        this.bankAccountDAO = bankAccountDAO;
        this.service = service;
    }

    public List<Payment> getAllPayments() {
        return paymentDAO.findAll();
    }

    public Payment getPaymentByID(Integer valueOf) {
        Optional<Payment> payment = paymentDAO.findById(valueOf);
        if (payment.isPresent()) return payment.get();
        return Payment.defaultPayment;
    }

    public Payment createNewPayment(Payment payment) {
        BankAccount bankAccount = getNeededBankAccountOrThrowException(payment);
        if (bankAccount.getBalance() < payment.getPrice()) throw new NotEnoughMoneyException();
        Optional<Payment> utilPayment = utilPayment = paymentDAO.save(payment);
        if (utilPayment.isEmpty()) throw new DatabaseException();

        bankAccount.setBalance(bankAccount.getBalance() - payment.getPrice());

        service.updateBankAccount(bankAccount.getId(), bankAccount);
        Optional<BankAccount> bankAccountOptional = bankAccountDAO.findById(bankAccount.getId());
        if (bankAccountOptional.isPresent()) {
            return utilPayment.get();
        } else {
            paymentDAO.deleteById(utilPayment.get().getId());
            throw new DatabaseException();
        }
    }

    private BankAccount getNeededBankAccountOrThrowException(Payment payment) {
        Optional<BankAccount> received = Optional.empty();
        if (payment.getPrice() == 0 || payment.getBankAccount().getBankAccountNumber() == 0)
            throw new NotEnoughDataException();
        List<BankAccount> all = bankAccountDAO.findAll();
        if (all.isEmpty()) throw new DatabaseException();
        for (BankAccount b : all) {
            if (b.getBankAccountNumber() == payment.getBankAccount().getBankAccountNumber()) {
                received = Optional.of(b);
            }
        }
        if (received.isEmpty()) throw new EntityNotFoundException();
        return received.get();
    }

    public void cancelPayment(Integer id) {
        if (id < 1) throw new IllegalArgumentException();
        Optional<Payment> payment = paymentDAO.findById(id);
        paymentDAO.deleteById(id);
        BankAccount bankAccount = payment.get().getBankAccount();
        bankAccount.setBalance(bankAccount.getBalance() + payment.get().getPrice());
        service.updateBankAccount(bankAccount.getId(), bankAccount);
    }

}
