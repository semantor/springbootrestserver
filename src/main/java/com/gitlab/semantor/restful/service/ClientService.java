package com.gitlab.semantor.restful.service;

import com.gitlab.semantor.restful.dao.BankAccountDAO;
import com.gitlab.semantor.restful.dao.ClientDAO;
import com.gitlab.semantor.restful.dao.PaymentDAO;
import com.gitlab.semantor.restful.entity.BankAccount;
import com.gitlab.semantor.restful.entity.Client;
import com.gitlab.semantor.restful.entity.Payment;
import com.gitlab.semantor.restful.exceptions.AlreadyExistException;
import com.gitlab.semantor.restful.exceptions.DatabaseException;
import com.gitlab.semantor.restful.exceptions.EntityNotFoundException;
import lombok.AccessLevel;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Component
public class ClientService {

    @Getter(AccessLevel.PROTECTED)
    private final ClientDAO clientDAO;
    @Getter(AccessLevel.PROTECTED)
    private final BankAccountDAO bankAccountDAO;
    @Getter(AccessLevel.PROTECTED)
    private final PaymentDAO paymentDAO;

    public ClientService(ClientDAO clientDAO, BankAccountDAO bankAccountDAO, PaymentDAO paymentDAO) {
        this.clientDAO = clientDAO;
        this.bankAccountDAO = bankAccountDAO;
        this.paymentDAO = paymentDAO;
    }

    public List<Client> getAllClient() {
        return clientDAO.findAll();
    }

    public Client getClientByID(Integer valueOf) {
        Optional<Client> receivedClient = clientDAO.findById(valueOf);
        if (receivedClient.isPresent()) return receivedClient.get();
        else throw new EntityNotFoundException();
    }

    public List<BankAccount> getBankAccountByClient(Integer valueOf) {
        return bankAccountDAO.findByClient_Id(valueOf);
    }

    public List<Payment> getClientPayments(Integer valueOf) {
        return paymentDAO.findByBankAccount_Client_Id(valueOf);
    }

    public Client createNewClient(Client newClient) {
        List<Client> allClient = getAllClient();
        for (Client c : allClient) {
            if (c.equals(newClient)) throw new AlreadyExistException();
        }
        Optional<Client> receivedClient = clientDAO.save(newClient);
        if (receivedClient.isEmpty()) throw new DatabaseException();
        return receivedClient.get();
    }

    public Client updateClientByID(Integer valueOf, Client newClient) {
        if (clientDAO.findById(valueOf).isEmpty()) throw new EntityNotFoundException();
        Optional<Client> client = clientDAO.findById(valueOf);
        client.get().setName(newClient.getName());
        client.get().setSurname(newClient.getSurname());
        client.get().setBirthday(newClient.getBirthday());
        clientDAO.save(client.get());
        client = clientDAO.findById(valueOf);
        if (client.isEmpty()) throw new DatabaseException();
        return client.get();
    }

    public void deleteClientByID(Integer id) {
        if (id < 1) throw new IllegalArgumentException();
        clientDAO.deleteById(id);
    }

    public List<Client> getClientByName(String name) {
        return clientDAO.findByNameOrSurname(name);
    }

    public List<Client> getYoungClient(Date date) {
        return clientDAO.findWhereBirthdayMoreThen(date);
    }
}
