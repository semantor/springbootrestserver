package com.gitlab.semantor.restful.service;

import com.gitlab.semantor.restful.dao.BankAccountDAO;
import com.gitlab.semantor.restful.dao.ClientDAO;
import com.gitlab.semantor.restful.dao.PaymentDAO;
import com.gitlab.semantor.restful.entity.BankAccount;
import com.gitlab.semantor.restful.entity.Client;
import com.gitlab.semantor.restful.entity.Payment;
import com.gitlab.semantor.restful.exceptions.DatabaseException;
import com.gitlab.semantor.restful.exceptions.EntityNotFoundException;
import lombok.AccessLevel;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class BankAccountService {

    @Getter(AccessLevel.PROTECTED)
    private final BankAccountDAO bankAccountDAO;
    @Getter(AccessLevel.PROTECTED)
    private final PaymentDAO paymentDAO;
    @Getter(AccessLevel.PROTECTED)
    private final ClientDAO clientDAO;

    public BankAccountService(BankAccountDAO bankAccountDAO, PaymentDAO paymentDAO, ClientDAO clientDAO) {
        this.bankAccountDAO = bankAccountDAO;
        this.paymentDAO = paymentDAO;
        this.clientDAO = clientDAO;
    }

    public List<BankAccount> getAllBankAccounts() {
        return bankAccountDAO.findAll();
    }

    public BankAccount getBankAccountByID(int valueOf) {
        Optional<BankAccount> bankAccount = bankAccountDAO.findById(valueOf);
        if (bankAccount.isEmpty()) throw new EntityNotFoundException();
        return bankAccount.get();
    }

    public List<Payment> getPaymentByBankAccount(int valueOf) {
        return paymentDAO.findByBankAccount_Id(valueOf);
    }

    public BankAccount createNewBankAccount(int clientID) {
        Optional<Client> client = clientDAO.findById(clientID);
        if (client.isEmpty()) throw new EntityNotFoundException();
        BankAccount bankAccount = new BankAccount(generateNewBankAccountNumber(), 0, client.get());
        Optional<BankAccount> newBankAccount = bankAccountDAO.save(bankAccount);
        if (newBankAccount.isEmpty()) throw new DatabaseException();
        return newBankAccount.get();

    }

    public BankAccount updateBankAccount(Integer valueOf, BankAccount newBankAccount) {
        if (bankAccountDAO.findById(valueOf).isEmpty()) throw new EntityNotFoundException();
        Optional<BankAccount> bankAccount = bankAccountDAO.findById(valueOf);
        bankAccount.get().setBalance(newBankAccount.getBalance());
        bankAccount.get().setClient(newBankAccount.getClient());
        Optional<BankAccount> receivedBA = bankAccountDAO.save(bankAccount.get());
        if (receivedBA.isEmpty()) throw new DatabaseException();
        return receivedBA.get();
    }

    public void deleteBankAccount(Integer id) {
        if (id < 1) throw new IllegalArgumentException();
        bankAccountDAO.deleteById(id);
    }

    long generateNewBankAccountNumber() {
        return 1000_0000_0000_0000L + Math.round(Math.random() * 9000_0000_0000_0000L);
    }


}
