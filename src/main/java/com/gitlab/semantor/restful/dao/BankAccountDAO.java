package com.gitlab.semantor.restful.dao;


import com.gitlab.semantor.restful.entity.BankAccount;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface BankAccountDAO extends Repository<BankAccount, Integer> {

    List<BankAccount> findAll();

    Optional<BankAccount> findById(Integer id);

    Optional<BankAccount> save(BankAccount account);

    Optional<BankAccount> deleteById(Integer id);

    List<BankAccount> findByClient_Id(Integer id);

}
