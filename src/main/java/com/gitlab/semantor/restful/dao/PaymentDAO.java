package com.gitlab.semantor.restful.dao;

import com.gitlab.semantor.restful.entity.Payment;
import com.gitlab.semantor.restful.entity.Status;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface PaymentDAO extends Repository<Payment, Integer> {
    List<Payment> findAll();

    Optional<Payment> findById(Integer id);

    Optional<Payment> save(Payment account);

    Optional<Payment> deleteById(Integer id);

    List<Payment> findByBankAccount_Client_Id(Integer id);

    List<Payment> findByBankAccount_Id(Integer id);

    @Modifying
    @Query("update Payment t set t.status = :status where t.id = :id")
    void setStatus(@Param("id") Integer id, @Param("status") Status status);

}
