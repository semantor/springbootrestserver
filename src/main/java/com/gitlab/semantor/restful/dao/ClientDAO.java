package com.gitlab.semantor.restful.dao;

import com.gitlab.semantor.restful.entity.Client;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

public interface ClientDAO extends Repository<Client, Integer> {

    List<Client> findAll();

    Optional<Client> findById(Integer id);

    Optional<Client> save(Client account);

    Optional<Client> deleteById(Integer id);

    @Query("select c from Client c where c.name = :name")
    List<Client> findByNameOrSurname(@Param("name") String name);

    @Query("select c from Client c where c.birthday>:date")
    List<Client> findWhereBirthdayMoreThen(@Param("date") Date date);


}
